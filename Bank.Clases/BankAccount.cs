using System;

namespace Bank.Clases
{
    public class BankAccount
    {
        public enum BankAccountType
        {
            SAVINGS,
            CHECKINGS
        }

        private BankAccountType Type;
        private int Balance;

        public BankAccount(BankAccountType type)
        {
            this.Type = type;
            this.Balance = 0;
        }
        public bool CanWithdraw(int amount)
        {
            if (Type == BankAccountType.SAVINGS)
            {
                return Balance - amount > 0;
            }
            else
            {
                return true;
            }
        }

        public static BankAccount CreateSavingAccount()
        {
            return new BankAccount(BankAccountType.SAVINGS);
        }
        public static BankAccount CreateChekingAccount()
        {
            return new BankAccount(BankAccountType.CHECKINGS);
        }
    }
}
